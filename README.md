# 30 Ray Simulated Undersampling

This notebook drops one ray at a time from every frame and reconstructs in order to simulate greater undersampling.

we use scale factors from corresponding 30ray set to be consistent, recalculating them here gives skewed results since most of the data set is highly undersampled and the scaling method is written to normalize across time frames, not datasets.

### Note on packages

This notebook only works if the dependent packages are on the path. In fact, when you call `run` one of the first things that happens is it checks that the needed packages are available at the right major version numbers.

You need to download those packages, put them on the path, and make sure the package name matches exactly with `+` plus sign in front to tell MATLAB it's a package.

### +Shared

For this notebook that means you need the +Shared package which is in the same project folder on bitbucket as this notebook.
