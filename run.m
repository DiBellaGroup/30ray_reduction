% Clear everything out
clear all
close all

% Add all files local to notebook and packages
addpath('..')
addpath('lib')
addpath('~/packages')

% Verify that needed packages are available
verify

% Constants
N_RAY_REDUCTION = 20; % How many rays to remove at max reduction
SPECIAL_TIME = 5;     % Which time frame will be stored and compared?

% pick the date folder to get data from
dataDate = '2015-12-09';

% Just slice 5 since this is time-consuming
iSlice = 5;
sliceString = num2str(iSlice);

% Load data - kSpaceSlice, trajectory
file = ['data/' dataDate '/30ray_gated_slice_' sliceString '.mat'];
load(file)


% Use scale factors from 30ray set to be consistent, recalculating them here
% is bound to give skewed results since most of the data set is too undersampled
% loads: grid3Scale, grogScale, nnScale
load('../+Shared/data/30_ray_scale_factors.mat')


% Pre-allocate `final` variables for loop
nReadouts = size(kSpaceSlice, 1);
grid3Final = zeros(nReadouts, nReadouts, N_RAY_REDUCTION);
grogFinal = grid3Final;
nnFinal = grid3Final;
nufftFinal = grid3Final;

grid3FinalScaled = grid3Final;
grogFinalScaled = grid3Final;
nnFinalScaled = grid3Final;
nufftFinalScaled = grid3Final;

% Ray reduction loop
for iRayReduction = 0:N_RAY_REDUCTION - 1
  % Display current level in loop
  display_loop_update(iRayReduction)

  % Chop off rays
  reducedKSpace = kSpaceSlice(:,1:end-iRayReduction,:,:);
  reducedTrajectory = trajectory(:,1:end-iRayReduction,:);

  % Reconstruct and Reorient
  Results = Shared.reconstruct_data(reducedKSpace, reducedTrajectory, iSlice);
  Results.grid3Image = fliplr(Results.grid3Image);
  Results.grogImage = fliplr(Results.grogImage);
  Results.nnImage = fliplr(Results.nnImage);
  Results.nufftImage = fliplr(Results.nufftImage);

  % Scale using factors loaded before loop ^^^
  Results.nufftScaled = Results.nufftImage * nufftScale;
  Results.grid3Scaled = Results.grid3Image * grid3Scale;
  Results.grogScaled = Results.grogImage * grogScale;
  Results.nnScaled = Results.nnImage * nnScale;

  % Store data as 1 frame in each both scaled and unscaled
  grid3Final(:,:,iRayReduction+1) = Results.grid3Image(:,:,SPECIAL_TIME);
  grogFinal(:,:,iRayReduction+1) = Results.grogImage(:,:,SPECIAL_TIME);
  nnFinal(:,:,iRayReduction+1) = Results.nnImage(:,:,SPECIAL_TIME);
  nufftFinal(:,:,iRayReduction+1) = Results.nufftImage(:,:,SPECIAL_TIME);

  grid3FinalScaled(:,:,iRayReduction+1) = Results.grid3Scaled(:,:,SPECIAL_TIME);
  grogFinalScaled(:,:,iRayReduction+1) = Results.grogScaled(:,:,SPECIAL_TIME);
  nnFinalScaled(:,:,iRayReduction+1) = Results.nnScaled(:,:,SPECIAL_TIME);
  nufftFinalScaled(:,:,iRayReduction+1) = Results.nufftScaled(:,:,SPECIAL_TIME);
end

% Store raw data with same names as others for uniform post-processing
Results.grid3Image = grid3Final;
Results.grogImage = grogFinal;
Results.nnImage = nnFinal;
Results.nufftImage = nufftFinal;
Results.grid3Scaled = grid3FinalScaled;
Results.grogScaled = grogFinalScaled;
Results.nnScaled = nnFinalScaled;
Results.nufftScaled = nufftFinalScaled;

% Write raw data
todaysDate = Shared.todays_date;
mkdir('results', todaysDate);
resultsPath = ['results/' todaysDate '/30_ray_reduced_slice_' sliceString '_'];
save(resultsPath, Results)
