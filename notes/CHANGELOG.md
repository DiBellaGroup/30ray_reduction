# v2.0.0 - Dec 9th, 2015

Massive change to structure and function of code, but the results should really be the same.

Oh, except for the NUFFT results, those were produced with new weights.
